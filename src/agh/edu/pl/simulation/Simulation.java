package agh.edu.pl.simulation;

import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

import agh.edu.pl.graph.Edge;
import agh.edu.pl.graph.Graph;
import agh.edu.pl.graph.changes.GraphChange;
import agh.edu.pl.simulation.action.Action;
import agh.edu.pl.simulation.action.ChangeGraphAction;
import agh.edu.pl.simulation.action.DepolyTransportUnitAction;
import agh.edu.pl.simulation.action.EndAction;
import agh.edu.pl.simulation.action.MoveAction;

public class Simulation {

    private PriorityQueue<SimultionEntry> simulationQueue = new PriorityQueue<Simulation.SimultionEntry>(5,
            new Comparator<SimultionEntry>() {
                @Override
                public int compare(SimultionEntry simultionEntry1, SimultionEntry simultionEntry2) {
                    int timeComparison = Double.compare(simultionEntry1.time, simultionEntry2.time);
                    return timeComparison == 0 ? simultionEntry1.priority - simultionEntry2.priority : timeComparison;
                }
            });

    private boolean simulationEnd;
    private boolean graphChanged;

    private Graph graph;

    private GraphChange graphChange;

    private double simulationEndTime;
    private double currentTime;

    public Simulation(Graph graph, double simulationTime) {
        this.simulationEndTime = simulationTime;
        simulationQueue.add(new SimultionEntry(simulationTime, new EndAction(this)));
        this.graph = graph;
    }

    public void start() {
        while (!simulationEnd) {
            performNextAction();
        }
    }

    public void addAction(Action action, double time) {
        simulationQueue.add(new SimultionEntry(time, action));
    }

    private void performNextAction() {
        SimultionEntry entry = simulationQueue.poll();
        currentTime = entry.time;
        entry.action.doAction();
        if (graphChanged) {
            updateMoveActionsTime();
            updateGraph();
            graphChanged = false;
        }
    }

    private void updateMoveActionsTime() {
        Iterator<SimultionEntry> actionsIt = simulationQueue.iterator();
        List<UpdateEntry> updateList = new LinkedList<UpdateEntry>();
        SimultionEntry simultionEntry;
        Action action;
        double updatedTime;
        while (actionsIt.hasNext()) {
            simultionEntry = actionsIt.next();
            action = simultionEntry.action;
            if (action instanceof MoveAction) {
                updatedTime = getUpdatedTime(simultionEntry);
                if (updatedTime != simultionEntry.time) {
                    updateList.add(new UpdateEntry(simultionEntry, updatedTime));
                }
            }
        }

        for (UpdateEntry updateEntry : updateList) {
            simulationQueue.remove(updateEntry.simultionEntry);
        }

        for (UpdateEntry updateEntry : updateList) {
            addAction(updateEntry.simultionEntry.action, updateEntry.updatedTime);
        }
    }

    private void updateGraph() {
        for (Edge edge : graph.getEdges()) {
            if (edge == graphChange.getEdge()) {
                edge.setVelocity(graphChange.getVelocity());
                break;
            }
        }
    }

    private double getUpdatedTime(SimultionEntry simultionEntry) {
        MoveAction action = ((MoveAction) simultionEntry.action);
        Edge edge = action.getPreviousEdge();
        if (graphChange.getEdge() == edge) {
            return currentTime
                    + ((edge.getDistance() - edge.getVelocity() * (currentTime - action.getDepartureTime())) / graphChange
                            .getVelocity());
        }
        return simultionEntry.time;
    }

    public void endSimulation() {
        simulationEnd = true;
    }

    public void graphChanged(GraphChange change) {
        graphChange = change;
        graphChanged = true;
    }

    public double getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(double currentTime) {
        this.currentTime = currentTime;
    }

    public double getSimulationEndTime() {
        return simulationEndTime;
    }

    private class SimultionEntry {
        public double time;
        public Action action;
        public int priority;

        public SimultionEntry(double time, Action action) {
            super();
            this.time = time;
            this.action = action;
            this.priority = getActionPriority(action);
        }

        private int getActionPriority(Action action) {
            if (action instanceof ChangeGraphAction) {
                return 1;
            } else if (action instanceof DepolyTransportUnitAction) {
                return 2;
            } else if (action instanceof MoveAction) {
                return 3;
            } else if (action instanceof EndAction) {
                return 4;
            } else {
                return 5;
            }
        }
    }

    private class UpdateEntry {
        public SimultionEntry simultionEntry;
        public double updatedTime;

        public UpdateEntry(SimultionEntry simultionEntry, double updatedTime) {
            super();
            this.simultionEntry = simultionEntry;
            this.updatedTime = updatedTime;
        }
    }
}
