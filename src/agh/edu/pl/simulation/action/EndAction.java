package agh.edu.pl.simulation.action;

import agh.edu.pl.simulation.Simulation;

public class EndAction implements Action {

    private Simulation simulation;

    public EndAction(Simulation simulation) {
        super();
        this.simulation = simulation;
    }

    @Override
    public void doAction() {
        System.out.println("Simulation ended!");
        simulation.endSimulation();
    }

}
