package agh.edu.pl.simulation.action;

import agh.edu.pl.graph.changes.GraphChange;
import agh.edu.pl.graph.changes.GraphChangesIterator;
import agh.edu.pl.simulation.Simulation;

public class ChangeGraphAction implements Action {

    private Simulation simulation;
    private GraphChange graphChange;
    private GraphChangesIterator graphChangesIterator;
    private double cycleTime;

    public ChangeGraphAction(Simulation simulation, GraphChangesIterator graphChangesIterator) {
        super();
        this.simulation = simulation;
        this.graphChangesIterator = graphChangesIterator;
    }

    @Override
    public void doAction() {
        simulation.graphChanged(graphChange);

        ChangeGraphAction changeGraphAction = new ChangeGraphAction(simulation, graphChangesIterator);
        if (graphChangesIterator.hasNext()) {
            if (graphChangesIterator.isCyclic()) {
                GraphChange graphChange = new GraphChange();
                GraphChange tmpGraphChange = graphChangesIterator.next();
                graphChange.setEdge(tmpGraphChange.getEdge());
                graphChange.setTime(graphChangesIterator.graphChangeAbsoluteTime(tmpGraphChange));
                graphChange.setVelocity(tmpGraphChange.getVelocity());

                changeGraphAction.setGraphChange(graphChange);
                changeGraphAction.setCycleTime(cycleTime);

                simulation.addAction(changeGraphAction, graphChange.getTime());
            } else {
                GraphChange graphChange = graphChangesIterator.next();
                changeGraphAction.setGraphChange(graphChange);
                simulation.addAction(changeGraphAction, graphChange.getTime());
            }
        }
    }

    public GraphChange getGraphChange() {
        return graphChange;
    }

    public void setGraphChange(GraphChange graphChange) {
        this.graphChange = graphChange;
    }

    public double getCycleTime() {
        return cycleTime;
    }

    public void setCycleTime(double cycleTime) {
        this.cycleTime = cycleTime;
    }

}
