package agh.edu.pl.simulation.action;

import agh.edu.pl.graph.Edge;
import agh.edu.pl.simulation.Simulation;
import agh.edu.pl.transport.TransportUnit;

public class MoveAction implements Action {

    private Simulation simulation;
    private TransportUnit transportUnit;
    private Edge previousEdge;
    private double departureTime;

    public MoveAction(Simulation simulation) {
        this.simulation = simulation;
    }

    @Override
    public void doAction() {

        if (transportUnit.getDestinationNode() == previousEdge.getNodeTo()) {
            System.out.println(transportUnit.getName() + ": Package delivered! Total move time: "
                    + (simulation.getCurrentTime() - transportUnit.getDepartureTime()));
            transportUnit.destinationReached(simulation.getCurrentTime());
            return;
        }

        MoveAction moveAction = new MoveAction(simulation);
        moveAction.setTransportUnit(transportUnit);
        Edge nextEdge = transportUnit.getNextEdge(previousEdge.getNodeTo(), simulation.getCurrentTime());
        moveAction.setPreviousEdge(nextEdge);
        moveAction.setDepartureTime(simulation.getCurrentTime());

        simulation.addAction(moveAction, simulation.getCurrentTime() + nextEdge.getTime());
    }

    public TransportUnit getTransportUnit() {
        return transportUnit;
    }

    public void setTransportUnit(TransportUnit transportUnit) {
        this.transportUnit = transportUnit;
    }

    public Edge getPreviousEdge() {
        return previousEdge;
    }

    public void setPreviousEdge(Edge previousEdge) {
        this.previousEdge = previousEdge;
    }

    public double getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(double departureTime) {
        this.departureTime = departureTime;
    }
}
