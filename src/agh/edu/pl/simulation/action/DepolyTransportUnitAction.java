package agh.edu.pl.simulation.action;

import agh.edu.pl.graph.Edge;
import agh.edu.pl.graph.Node;
import agh.edu.pl.simulation.Simulation;
import agh.edu.pl.statistics.AverageMoveTimeStatistics;
import agh.edu.pl.transport.TransportUnit;
import agh.edu.pl.transport.logic.DecisionAlgorithm;

public class DepolyTransportUnitAction implements Action {

    private Node nodeA;
    private Node nodeB;
    private DecisionAlgorithm decisionAlgorithm;
    private AverageMoveTimeStatistics statistics;
    private String transportUnitName;

    private Simulation simulation;

    public DepolyTransportUnitAction(Simulation simulation) {
        this.simulation = simulation;
    }

    @Override
    public void doAction() {
        TransportUnit transportUnit = new TransportUnit(nodeA, nodeB, decisionAlgorithm, simulation.getCurrentTime());
        transportUnit.setName(transportUnitName);
        transportUnit.setAverageMoveTimeStatistics(statistics);

        Edge nextEdge = transportUnit.getNextEdge(nodeA, simulation.getCurrentTime());

        MoveAction moveAction = new MoveAction(simulation);
        moveAction.setDepartureTime(simulation.getCurrentTime());
        moveAction.setPreviousEdge(nextEdge);
        moveAction.setTransportUnit(transportUnit);

        simulation.addAction(moveAction, simulation.getCurrentTime() + nextEdge.getTime());
    }

    public Node getNodeA() {
        return nodeA;
    }

    public void setNodeA(Node nodeA) {
        this.nodeA = nodeA;
    }

    public Node getNodeB() {
        return nodeB;
    }

    public void setNodeB(Node nodeB) {
        this.nodeB = nodeB;
    }

    public DecisionAlgorithm getDecisionAlgorithm() {
        return decisionAlgorithm;
    }

    public void setDecisionAlgorithm(DecisionAlgorithm decisionAlgorithm) {
        this.decisionAlgorithm = decisionAlgorithm;
    }

    public String getTransportUnitName() {
        return transportUnitName;
    }

    public void setTransportUnitName(String transportUnitName) {
        this.transportUnitName = transportUnitName;
    }

    public AverageMoveTimeStatistics getStatistics() {
        return statistics;
    }

    public void setStatistics(AverageMoveTimeStatistics statistics) {
        this.statistics = statistics;
    }

}
