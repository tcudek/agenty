package agh.edu.pl.simulation.action;

import agh.edu.pl.graph.changes.GraphChanges;
import agh.edu.pl.graph.changes.estimation.CyclesAverageHistoryEstimator;
import agh.edu.pl.simulation.Simulation;

public class UpdateClassifierAction implements Action {

    private Simulation simulation;
    private CyclesAverageHistoryEstimator classifier;
    private GraphChanges graphChanges;
    private double cycleTime;

    public UpdateClassifierAction(Simulation simulation) {
        super();
        this.simulation = simulation;
    }

    @Override
    public void doAction() {
        classifier.updateClassificator(graphChanges, simulation.getCurrentTime(), cycleTime);
        UpdateClassifierAction updateClassifierAction = new UpdateClassifierAction(simulation);
        updateClassifierAction.setGraphChanges(graphChanges);
        updateClassifierAction.setClassifier(classifier);
        updateClassifierAction.setCycleTime(cycleTime);
        simulation.addAction(updateClassifierAction, simulation.getCurrentTime() + cycleTime);
    }

    public double getCycleTime() {
        return cycleTime;
    }

    public void setCycleTime(double cycleTime) {
        this.cycleTime = cycleTime;
    }

    public CyclesAverageHistoryEstimator getClassifier() {
        return classifier;
    }

    public void setClassifier(CyclesAverageHistoryEstimator classifier) {
        this.classifier = classifier;
    }

    public GraphChanges getGraphChanges() {
        return graphChanges;
    }

    public void setGraphChanges(GraphChanges graphChanges) {
        this.graphChanges = graphChanges;
    }

}
