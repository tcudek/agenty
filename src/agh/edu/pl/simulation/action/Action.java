package agh.edu.pl.simulation.action;

public interface Action {

    public void doAction();

}
