package agh.edu.pl.transport.logic;

import agh.edu.pl.graph.Edge;
import agh.edu.pl.graph.Graph;
import agh.edu.pl.graph.Node;
import agh.edu.pl.graph.algorithm.DijskraAlgorithm;
import agh.edu.pl.graph.changes.estimation.TimeEstimator;

public class DecisionIterativeDijskraAlgorithm implements DecisionAlgorithm {

    private DijskraAlgorithm dijskraAlgorithm;
    private Node destinationNode;

    public DecisionIterativeDijskraAlgorithm(Graph graph, TimeEstimator estimator) {
        dijskraAlgorithm = new DijskraAlgorithm(graph);
        dijskraAlgorithm.setTimeEstimator(estimator);
    }

    @Override
    public void prepare(Node startingNode, Node endingNode, double departureTime) {
        destinationNode = endingNode;
    }

    @Override
    public Edge getNextEdge(Node node, double departureTime) {
        return dijskraAlgorithm.getPath(node, destinationNode, departureTime).get(0);
    }
}
