package agh.edu.pl.transport.logic;

import agh.edu.pl.graph.Edge;
import agh.edu.pl.graph.Node;

public interface DecisionAlgorithm {

    public void prepare(Node startingNode, Node endingNode, double departureTime);

    public Edge getNextEdge(Node node, double departureTime);
}
