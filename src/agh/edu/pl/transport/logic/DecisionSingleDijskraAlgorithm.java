package agh.edu.pl.transport.logic;

import java.util.Collection;

import agh.edu.pl.graph.Edge;
import agh.edu.pl.graph.Graph;
import agh.edu.pl.graph.Node;
import agh.edu.pl.graph.algorithm.DijskraAlgorithm;
import agh.edu.pl.graph.changes.estimation.TimeEstimator;

public class DecisionSingleDijskraAlgorithm implements DecisionAlgorithm {
    private DijskraAlgorithm dijskraAlgorithm;

    private Collection<Edge> currentPath;

    public DecisionSingleDijskraAlgorithm(Graph graph, TimeEstimator timeEstimator) {
        dijskraAlgorithm = new DijskraAlgorithm(graph);
        dijskraAlgorithm.setTimeEstimator(timeEstimator);
    }

    @Override
    public void prepare(Node startingNode, Node endingNode, double departureTime) {
        currentPath = dijskraAlgorithm.getPath(startingNode, endingNode, departureTime);
    }

    @Override
    public Edge getNextEdge(Node node, double departureTime) {
        return getEdgeByStartingNode(node);
    }

    private Edge getEdgeByStartingNode(Node node) {
        for (Edge edge : currentPath) {
            if (edge.getNodeFrom() == node) {
                return edge;
            }
        }
        return null;
    }

}
