package agh.edu.pl.transport;

import agh.edu.pl.graph.Edge;
import agh.edu.pl.graph.Node;
import agh.edu.pl.statistics.AverageMoveTimeStatistics;
import agh.edu.pl.transport.logic.DecisionAlgorithm;

public class TransportUnit {

    private String name;

    private DecisionAlgorithm decisionAlgorithm;

    private AverageMoveTimeStatistics averageMoveTimeStatistics;

    private Node destinationNode;

    private Node nodeA = new Node();
    private Node nodeB = new Node();

    private double moveTime;
    private double departureTime;

    private boolean isPrepared;

    public TransportUnit(Node nodeA, Node nodeB, DecisionAlgorithm decisionAlgorithm, double departureTime) {
        super();
        this.nodeA = nodeA;
        this.nodeB = nodeB;
        this.decisionAlgorithm = decisionAlgorithm;
        this.departureTime = departureTime;

        destinationNode = nodeB;

        decisionAlgorithm.prepare(nodeA, nodeB, departureTime);
    }

    private void updateCurrentPath(Node startingNode, Node destinationNode, double departureTime) {
        decisionAlgorithm.prepare(startingNode, destinationNode, departureTime);
    }

    public Node getDestinationNode() {
        return destinationNode;
    }

    public void setDestinationNode(Node destinationNode) {
        this.destinationNode = destinationNode;
    }

    public void setNodeA(Node nodeA) {
        this.nodeA = nodeA;
    }

    public void setNodeB(Node nodeB) {
        this.nodeB = nodeB;
    }

    public void destinationReached(double time) {
        if (destinationNode == null) {
            destinationNode = nodeB;
            updateCurrentPath(nodeA, nodeB, time);
        } else {
            if (destinationNode == nodeA) {
                destinationNode = nodeB;
                updateCurrentPath(nodeB, nodeA, time);
            } else {
                destinationNode = nodeA;
                updateCurrentPath(nodeA, nodeB, time);
            }
        }
        moveTime = 0;
        averageMoveTimeStatistics.addMoveTime(time - departureTime);
    }

    public Edge getNextEdge(Node node, double time) {
        return decisionAlgorithm.getNextEdge(node, time);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDepartureTime() {
        return departureTime;
    }

    public AverageMoveTimeStatistics getAverageMoveTimeStatistics() {
        return averageMoveTimeStatistics;
    }

    public void setAverageMoveTimeStatistics(AverageMoveTimeStatistics averageMoveTimeStatistics) {
        this.averageMoveTimeStatistics = averageMoveTimeStatistics;
    }

}
