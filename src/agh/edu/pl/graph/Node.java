package agh.edu.pl.graph;

import java.util.LinkedList;
import java.util.List;

public class Node {

    private int id;

    private List<Edge> edgesOut = new LinkedList<Edge>();
    private List<Edge> edgesIn = new LinkedList<Edge>();

    public Node() {
        // TODO Auto-generated constructor stub
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Edge> getEdgesOut() {
        return edgesOut;
    }

    public void setEdgesOut(List<Edge> edgesOut) {
        this.edgesOut = edgesOut;
    }

    public List<Edge> getEdgesIn() {
        return edgesIn;
    }

    public void setEdgesIn(List<Edge> edgesIn) {
        this.edgesIn = edgesIn;
    }

    public Edge getEdgeToNode(Node node) {
        for (Edge edge : edgesOut) {
            if (edge.getNodeTo() == node) {
                return edge;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "node" + id;
    }

}
