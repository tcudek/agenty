package agh.edu.pl.graph.algorithm;

import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

import agh.edu.pl.graph.Edge;
import agh.edu.pl.graph.Graph;
import agh.edu.pl.graph.Node;
import agh.edu.pl.graph.changes.estimation.TimeEstimator;

public class DijskraAlgorithm {

    private Graph graph;

    private TimeEstimator timeEstimator;

    public DijskraAlgorithm(Graph graph) {
        super();
        this.graph = graph;
    }

    public List<Edge> getPath(Node startingNode, Node destinationNode, double departureTime) {

        PriorityQueue<DijskraEntry> nodes = new PriorityQueue<DijskraEntry>(graph.getNodes().size(),
                new Comparator<DijskraEntry>() {
                    @Override
                    public int compare(DijskraEntry dijskraEntry1, DijskraEntry dijskraEntry2) {
                        return (int) (dijskraEntry1.cost - dijskraEntry2.cost);
                    }
                });

        DijskraEntry startingNodeEntry = initializeQueue(nodes, startingNode, departureTime);

        DijskraEntry entry = null;
        double costFromRoot;

        while (nodes.size() > 0) {
            entry = nodes.poll();

            if (entry.node == destinationNode) {
                break;
            }

            for (DijskraEntry neighbour : getNotRemovedNeighbours(entry, nodes)) {

                costFromRoot = entry.cost
                        + timeEstimator.estimateTime(entry.cost, entry.node.getEdgeToNode(neighbour.node));

                if (costFromRoot < neighbour.cost) {
                    nodes.remove(neighbour);
                    neighbour.cost = costFromRoot;
                    neighbour.previous = entry;
                    nodes.add(neighbour);
                }
            }
        }

        /* === Build path === */
        LinkedList<Edge> edges = new LinkedList<Edge>();
        // if (entry.node != destinationNode) {
        // edges.add(graph.getEdgeBetweenNodes(lastRemovedEntry.node,
        // destinationNode));
        // entry = lastRemovedEntry;
        while (entry != startingNodeEntry) {
            edges.addFirst(graph.getEdgeBetweenNodes(entry.previous.node, entry.node));
            entry = entry.previous;
        }
        // }

        return edges;
    }

    private DijskraEntry initializeQueue(PriorityQueue<DijskraEntry> queue, Node startingNode, double departureTime) {
        DijskraEntry startingNodeEntry = null;

        for (Node node : graph.getNodes()) {
            if (node == startingNode) {
                startingNodeEntry = new DijskraEntry(node, departureTime, null);
                queue.add(startingNodeEntry);
            } else {
                queue.add(new DijskraEntry(node, Double.MAX_VALUE, null));
            }
        }

        return startingNodeEntry;
    }

    private Collection<DijskraEntry> getNotRemovedNeighbours(DijskraEntry entry, Collection<DijskraEntry> entries) {
        Collection<DijskraEntry> neighbours = new LinkedList<DijskraAlgorithm.DijskraEntry>();
        for (Edge edge : entry.node.getEdgesOut()) {
            DijskraEntry tmpEntry = getDisjkraEntryByNode(edge.getNodeTo(), entries);
            if (tmpEntry != null)
                neighbours.add(tmpEntry);
        }
        return neighbours;
    }

    private DijskraEntry getDisjkraEntryByNode(Node node, Collection<DijskraEntry> entries) {
        for (DijskraEntry entry : entries) {
            if (entry.node == node) {
                return entry;
            }
        }
        return null;
    }

    private class DijskraEntry {
        public Node node;
        public double cost;
        public DijskraEntry previous;

        public DijskraEntry(Node node, double distance, DijskraEntry previous) {
            super();
            this.node = node;
            this.cost = distance;
            this.previous = previous;
        }

    }

    public TimeEstimator getTimeEstimator() {
        return timeEstimator;
    }

    public void setTimeEstimator(TimeEstimator timeEstimator) {
        this.timeEstimator = timeEstimator;
    }

}
