package agh.edu.pl.graph;

import java.util.List;

public class Graph {

    private List<Node> nodes;
    private List<Edge> edges;

    public Graph() {
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge> edges) {
        this.edges = edges;
    }

    public Node getNodeById(int nodeId) {
        for (Node node : nodes) {
            if (node.getId() == nodeId) {
                return node;
            }
        }
        return null;
    }

    public Edge getEdgeBetweenNodes(int nodeFromId, int nodeToId) {
        for (Edge edge : edges) {
            if (edge.getNodeFrom().getId() == nodeFromId && edge.getNodeTo().getId() == nodeToId) {
                return edge;
            }
        }
        return null;
    }

    public Edge getEdgeBetweenNodes(Node nodeFrom, Node nodeTo) {
        for (Edge edge : edges) {
            if (edge.getNodeFrom() == nodeFrom && edge.getNodeTo() == nodeTo) {
                return edge;
            }
        }
        return null;
    }

}
