package agh.edu.pl.graph;

public class Edge {

    private Node nodeFrom;
    private Node nodeTo;

    private double distance;
    private double velocity;

    public Node getNodeFrom() {
        return nodeFrom;
    }

    public void setNodeFrom(Node nodeFrom) {
        this.nodeFrom = nodeFrom;
    }

    public Node getNodeTo() {
        return nodeTo;
    }

    public void setNodeTo(Node nodeTo) {
        this.nodeTo = nodeTo;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getVelocity() {
        return velocity;
    }

    public void setVelocity(double velocity) {
        this.velocity = velocity;
    }

    public double getTime() {
        return distance / velocity;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("edge").append(nodeFrom.getId()).append("->").append(nodeTo.getId())
                .toString();
    }
}
