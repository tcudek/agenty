package agh.edu.pl.graph.xml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import agh.edu.pl.graph.Edge;
import agh.edu.pl.graph.Graph;

public class GraphXmlReader {

    public static Graph buildGraph(String xmlFilePath) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = null;

        Graph graph = new Graph();

        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(xmlFilePath);
            document.getDocumentElement().normalize();

            NodeList nodesChildren = document.getElementsByTagName("node");

            List<agh.edu.pl.graph.Node> graphNodes = new ArrayList<agh.edu.pl.graph.Node>(nodesChildren.getLength());

            for (int i = 0; i < nodesChildren.getLength(); ++i) {
                agh.edu.pl.graph.Node graphNode = new agh.edu.pl.graph.Node();
                Node node = nodesChildren.item(i);
                graphNode.setId(Integer.parseInt(node.getAttributes().getNamedItem("id").getTextContent()));
                graphNodes.add(graphNode);
            }

            graph.setNodes(graphNodes);

            nodesChildren = document.getElementsByTagName("edge");

            List<Edge> graphEdges = new ArrayList<Edge>(nodesChildren.getLength());

            for (int i = 0; i < nodesChildren.getLength(); ++i) {
                Edge graphEdge = new Edge();
                Node node = nodesChildren.item(i);

                NamedNodeMap attributes = node.getAttributes();

                agh.edu.pl.graph.Node graphNode = graph.getNodeById(Integer.parseInt(attributes.getNamedItem("from")
                        .getTextContent()));

                graphEdge.setNodeFrom(graphNode);
                graphNode.getEdgesOut().add(graphEdge);

                graphNode = graph.getNodeById(Integer.parseInt(attributes.getNamedItem("to").getTextContent()));

                graphEdge.setNodeTo(graphNode);
                graphNode.getEdgesIn().add(graphEdge);

                graphEdge.setDistance(Integer.parseInt(attributes.getNamedItem("distance").getTextContent()));

                graphEdge.setVelocity(Integer.parseInt(attributes.getNamedItem("velocity").getTextContent()));

                graphEdges.add(graphEdge);
            }

            graph.setEdges(graphEdges);

        } catch (ParserConfigurationException exception) {
            throw new RuntimeException(exception);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        } catch (SAXException exception) {
            throw new RuntimeException(exception);
        }
        return graph;
    }
}
