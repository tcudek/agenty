package agh.edu.pl.graph.changes.xml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import agh.edu.pl.graph.Edge;
import agh.edu.pl.graph.Graph;
import agh.edu.pl.graph.changes.GraphChange;
import agh.edu.pl.graph.changes.GraphChanges;

public class GraphChangesXmlReader {

    public static GraphChanges buildGraphChanges(String xmlFilePath, Graph graph) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = null;

        GraphChanges graphChanges = new GraphChanges();
        List<GraphChange> graphChangesList = null;

        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(xmlFilePath);
            document.getDocumentElement().normalize();

            NodeList graphChangesChildren = document.getElementsByTagName("change");

            graphChangesList = new ArrayList<GraphChange>(graphChangesChildren.getLength());

            for (int i = 0; i < graphChangesChildren.getLength(); ++i) {
                GraphChange graphChange = new GraphChange();
                Node node = graphChangesChildren.item(i);

                int nodeFromId = Integer.parseInt(node.getAttributes().getNamedItem("nodeFrom").getTextContent());
                int nodeToId = Integer.parseInt(node.getAttributes().getNamedItem("nodeTo").getTextContent());
                Edge edge = graph.getEdgeBetweenNodes(nodeFromId, nodeToId);
                if (edge == null) {
                    throw new NullPointerException("The edge beetween nodes " + nodeFromId + " and " + nodeToId
                            + " doeas not exist!");
                }
                graphChange.setEdge(edge);
                graphChange.setTime(Double.parseDouble(node.getAttributes().getNamedItem("time").getTextContent()));
                graphChange.setVelocity(Double.parseDouble(node.getAttributes().getNamedItem("velocity")
                        .getTextContent()));
                graphChangesList.add(graphChange);
            }

            Collections.sort(graphChangesList, new Comparator<GraphChange>() {

                @Override
                public int compare(GraphChange o1, GraphChange o2) {
                    return Double.compare(o1.getTime(), o2.getTime());
                }

            });

            graphChanges.setGraphChanges(graphChangesList);

            NodeList cycle = document.getElementsByTagName("cycle");

            if (cycle.getLength() == 1) {
                graphChanges.setCyclic(true);
                graphChanges.setCycleTime(Integer.parseInt(cycle.item(0).getAttributes().getNamedItem("duration")
                        .getTextContent()));
            }

        } catch (ParserConfigurationException exception) {
            throw new RuntimeException(exception);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        } catch (SAXException exception) {
            throw new RuntimeException(exception);
        }

        return graphChanges;
    }
}
