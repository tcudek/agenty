package agh.edu.pl.graph.changes;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class GraphChanges {

    private List<GraphChange> graphChanges;
    private double cycleTime;

    private boolean isCyclic;

    public List<GraphChange> getGraphChanges() {
        return graphChanges;
    }

    public void setGraphChanges(List<GraphChange> graphChanges) {
        this.graphChanges = graphChanges;
    }

    public double getCycleTime() {
        return cycleTime;
    }

    public void setCycleTime(double cycleTime) {
        this.cycleTime = cycleTime;
    }

    public boolean isCyclic() {
        return isCyclic;
    }

    public void setCyclic(boolean isCyclic) {
        this.isCyclic = isCyclic;
    }

    public GraphChangesIterator graphChangesIterator() {
        return new GraphChangesIterator(this);
    }

    public Collection<GraphChange> getGraphChangesToTime(double time) {
        Collection<GraphChange> graphChanges = new LinkedList<GraphChange>();
        if (this.graphChanges.size() < 1) {
            return graphChanges;
        }
        int iteration = 0;
        double cycleTime = getCycleTime();
        double base;
        if (isCyclic()) {
            overloop: while (true) {
                base = cycleTime * iteration;
                for (GraphChange change : this.graphChanges) {
                    if (change.getTime() + base <= time) {
                        GraphChange graphChange = new GraphChange();
                        graphChange.setEdge(change.getEdge());
                        graphChange.setTime(base + change.getTime());
                        graphChange.setVelocity(change.getVelocity());
                        graphChanges.add(graphChange);
                    } else {
                        break overloop;
                    }
                }
                ++iteration;
            }
        }
        return graphChanges;
    }

    public Collection<GraphChange> getGraphChangesBetweenTimes(double fromTime, double toTime) {
        Collection<GraphChange> graphChanges = new LinkedList<GraphChange>();
        if (this.graphChanges.size() < 1) {
            return graphChanges;
        }
        if (isCyclic()) {
            int iteration = 0;
            double cycleTime = getCycleTime();
            double base;
            overloop: while (true) {
                base = cycleTime * iteration;
                for (GraphChange change : this.graphChanges) {
                    if (change.getTime() + base >= fromTime) {
                        if (change.getTime() + base < toTime) {
                            GraphChange graphChange = new GraphChange();
                            graphChange.setEdge(change.getEdge());
                            graphChange.setTime(base + change.getTime());
                            graphChange.setVelocity(change.getVelocity());
                            graphChanges.add(graphChange);
                        } else {
                            break overloop;
                        }
                    }
                }
                ++iteration;
            }
            return graphChanges;
        } else {
            for (GraphChange change : this.graphChanges) {
                if (change.getTime() <= toTime && change.getTime() >= fromTime) {
                    GraphChange graphChange = new GraphChange();
                    graphChange.setEdge(change.getEdge());
                    graphChange.setTime(change.getTime());
                    graphChange.setVelocity(change.getVelocity());
                    graphChanges.add(graphChange);
                }
            }
            return graphChanges;
        }

    }

    public int getHowManyTimesGraphChanged(double time) {
        double changeTime = -1;
        int counter = 0;
        for (GraphChange change : getGraphChangesToTime(changeTime)) {
            if (change.getTime() != changeTime) {
                changeTime = change.getTime();
                ++counter;
            }
        }
        return counter;
    }

    public int getLastGraphChangeToTimeIndex(double time) {
        int index = -1;
        for (GraphChange graphChange : graphChanges) {
            if (graphChange.getTime() <= time) {
                ++index;
            } else {
                break;
            }
        }
        return index;
    }
}
