package agh.edu.pl.graph.changes;

import java.util.NoSuchElementException;

public class GraphChangesIterator {

    private GraphChanges graphChanges;
    private int cycleIteration;
    private int index;

    GraphChangesIterator(GraphChanges graphChanges) {
        this.graphChanges = graphChanges;
    }

    private boolean isIndexOutOfArray() {
        return index == graphChanges.getGraphChanges().size();
    }

    public boolean hasNext() {
        if (isCyclic()) {
            return true;
        } else {
            return !isIndexOutOfArray();
        }
    }

    public GraphChange next() {
        try {
            if (isIndexOutOfArray() && isCyclic()) {
                ++cycleIteration;
                index = 0;
            }
            return graphChanges.getGraphChanges().get(index++);
        } catch (IndexOutOfBoundsException exception) {
            throw new NoSuchElementException("Next element does not exist.");
        }
    }

    public double graphChangeAbsoluteTime(GraphChange graphChange) {
        if (graphChanges.isCyclic()) {
            return graphChange.getTime() + cycleIteration * graphChanges.getCycleTime();
        } else {
            return graphChange.getTime();
        }
    }

    public boolean isCyclic() {
        return graphChanges.isCyclic();
    }

    public double getCycleTime() {
        return graphChanges.getCycleTime();
    }

    public int getCycleIteration() {
        return cycleIteration;
    }
}
