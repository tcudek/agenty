package agh.edu.pl.graph.changes;

import agh.edu.pl.graph.Edge;

public class GraphChange {

    private Edge edge;
    private double time;
    private double velocity;

    public Edge getEdge() {
        return edge;
    }

    public void setEdge(Edge edge) {
        this.edge = edge;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    public double getVelocity() {
        return velocity;
    }

    public void setVelocity(double weight) {
        this.velocity = weight;
    }

}
