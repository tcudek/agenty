package agh.edu.pl.graph.changes.estimation;

import java.util.HashMap;
import java.util.Map;

import agh.edu.pl.graph.Edge;
import agh.edu.pl.graph.Graph;
import agh.edu.pl.graph.changes.GraphChange;
import agh.edu.pl.graph.changes.GraphChanges;
import agh.edu.pl.simulation.Simulation;

public class TimeWeightedAverageEstimator implements TimeEstimator {

    private Map<Edge, Double> edgeCostsMap = new HashMap<Edge, Double>();
    private Graph graph;
    private double simulationEndTime;

    public TimeWeightedAverageEstimator(Graph graph, Simulation simulation, GraphChanges graphChanges,
            double simulationEndTime) {
        this.simulationEndTime = simulationEndTime;
        this.graph = graph;
        setUpCostMap(graphChanges);
    }

    private void setUpCostMap(GraphChanges graphChanges) {
        for (Edge edge : graph.getEdges()) {
            edgeCostsMap.put(edge, getAverageTime(edge, graphChanges));
        }
    }

    private double getAverageTime(Edge edge, GraphChanges graphChanges) {
        double averageTime = 0;

        double distance = edge.getDistance();

        double velocity = edge.getVelocity();
        double lastChangeTime = 0;

        for (GraphChange graphChange : graphChanges.getGraphChanges()) {
            if (graphChange.getEdge() == edge) {
                averageTime += (distance / velocity) * (graphChange.getTime() - lastChangeTime);

                lastChangeTime = graphChange.getTime();
                velocity = graphChange.getVelocity();
            }
        }
        averageTime += (distance / velocity) * (simulationEndTime - lastChangeTime);
        return averageTime / simulationEndTime;
    }

    @Override
    public double estimateTime(double departureTime, Edge edge) {
        return edgeCostsMap.get(edge);
    }

}
