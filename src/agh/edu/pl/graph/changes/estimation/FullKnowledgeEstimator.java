package agh.edu.pl.graph.changes.estimation;

import agh.edu.pl.graph.Edge;
import agh.edu.pl.graph.changes.GraphChange;
import agh.edu.pl.graph.changes.GraphChanges;
import agh.edu.pl.graph.changes.GraphChangesIterator;

public class FullKnowledgeEstimator implements TimeEstimator {

    private GraphChanges graphChanges;

    public FullKnowledgeEstimator() {
    }

    @Override
    public double estimateTime(double departureTime, Edge edge) {
        Edge tmpEdge = new Edge();
        tmpEdge.setDistance(edge.getDistance());
        tmpEdge.setVelocity(edge.getVelocity());

        GraphChangesIterator changesIt = graphChanges.graphChangesIterator();

        GraphChange graphChange = null;
        while (changesIt.hasNext()) {
            GraphChange graphChangeTmp = changesIt.next();
            if (changesIt.graphChangeAbsoluteTime(graphChangeTmp) > departureTime) {
                graphChange = graphChangeTmp;
                break;
            }
            if (graphChangeTmp.getEdge() == edge) {
                tmpEdge.setVelocity(graphChangeTmp.getVelocity());
            }
        }

        double arrivalTime = departureTime + tmpEdge.getTime();

        if (graphChange == null) {
            return arrivalTime - departureTime;
        }

        {
            double graphChangeTmpAbsoluteTime = changesIt.graphChangeAbsoluteTime(graphChange);
            double reaminingDistance = tmpEdge.getDistance();
            double lastChangeTime = departureTime;
            double currentVelocity = tmpEdge.getVelocity();
            while (changesIt.hasNext()) {
                if (graphChangeTmpAbsoluteTime < arrivalTime) {
                    if (graphChange.getEdge() == edge) {
                        reaminingDistance = reaminingDistance - currentVelocity
                                * (graphChangeTmpAbsoluteTime - lastChangeTime);

                        arrivalTime = graphChangeTmpAbsoluteTime + reaminingDistance / graphChange.getVelocity();

                        currentVelocity = graphChange.getVelocity();
                        lastChangeTime = graphChangeTmpAbsoluteTime;
                    }
                } else {
                    break;
                }
                graphChange = changesIt.next();
                graphChangeTmpAbsoluteTime = changesIt.graphChangeAbsoluteTime(graphChange);
            }
        }

        return arrivalTime - departureTime;
    }

    public GraphChanges getGraphChanges() {
        return graphChanges;
    }

    public void setGraphChanges(GraphChanges graphChanges) {
        this.graphChanges = graphChanges;
    }
}
