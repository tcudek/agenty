package agh.edu.pl.graph.changes.estimation;

import agh.edu.pl.graph.Edge;

public class StandardEstimator implements TimeEstimator {

    @Override
    public double estimateTime(double departureTime, Edge edge) {
        return edge.getTime();
    }
}
