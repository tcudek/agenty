package agh.edu.pl.graph.changes.estimation;

import agh.edu.pl.graph.Edge;

public interface TimeEstimator {
    public double estimateTime(double departureTime, Edge edge);
}
