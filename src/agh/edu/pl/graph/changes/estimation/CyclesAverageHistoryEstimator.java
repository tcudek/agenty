package agh.edu.pl.graph.changes.estimation;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import agh.edu.pl.common.list.EvictableList;
import agh.edu.pl.common.timewindow.TimeWindow;
import agh.edu.pl.common.timewindow.TimeWindows;
import agh.edu.pl.common.timewindow.TimeWindowsCyclicIterator;
import agh.edu.pl.common.timewindow.merge.ContiniousAverageMerger;
import agh.edu.pl.graph.Edge;
import agh.edu.pl.graph.changes.GraphChange;
import agh.edu.pl.graph.changes.GraphChanges;

public class CyclesAverageHistoryEstimator implements TimeEstimator {

    private Map<Edge, TimeWindows> edgeCostsMap = new HashMap<Edge, TimeWindows>();
    private Map<Edge, Double> lastEdgesValues = new HashMap<Edge, Double>();
    private int historySize = 3;

    private Map<Edge, EvictableList<TimeWindows>> edgesHistory = new HashMap<Edge, EvictableList<TimeWindows>>();

    public void buildClassifier(Collection<Edge> edges) {
        for (Edge edge : edges) {
            lastEdgesValues.put(edge, edge.getVelocity());
            edgeCostsMap.put(edge, null);
            edgesHistory.put(edge, new EvictableList<TimeWindows>(historySize));
        }
    }

    @Override
    public double estimateTime(double departureTime, Edge edge) {
        TimeWindows timeWindows = edgeCostsMap.get(edge);

        if (timeWindows == null) {
            return edge.getDistance() / edge.getVelocity();
        }

        TimeWindowsCyclicIterator iterator = timeWindows.iterator(departureTime);
        TimeWindow timeWindow = iterator.next();

        double arrivalTime = departureTime + edge.getDistance() / timeWindow.velocity;

        double reaminingDistance = edge.getDistance();
        double lastChangeTime = departureTime;
        double currentVelocity = timeWindow.velocity;

        timeWindow = iterator.next();
        while (arrivalTime > timeWindow.startTime) {
            reaminingDistance = reaminingDistance - currentVelocity * (timeWindow.startTime - lastChangeTime);

            arrivalTime = timeWindow.startTime + reaminingDistance / timeWindow.velocity;
            currentVelocity = timeWindow.velocity;
            lastChangeTime = timeWindow.startTime;

            timeWindow = iterator.next();
        }

        return arrivalTime - departureTime;
    }

    public void updateClassificator(GraphChanges graphChanges, double time, double cycleTime) {
        Collection<GraphChange> graphChangesCollection = graphChanges.getGraphChangesBetweenTimes(time - cycleTime,
                time);
        double startTime = time - cycleTime;
        for (Entry<Edge, TimeWindows> entry : edgeCostsMap.entrySet()) {
            Edge edge = entry.getKey();
            TimeWindows timeWindows = createTimeWindowsForEdge(edge, graphChangesCollection, startTime, cycleTime);
            EvictableList<TimeWindows> history = edgesHistory.get(edge);
            history.add(timeWindows);
            ContiniousAverageMerger merger = new ContiniousAverageMerger();
            TimeWindows currentTimeWindows = new TimeWindows(timeWindows, merger);
            // if (currentTimeWindows == null) {
            // entry.setValue(timeWindows);
            // } else {
            for (TimeWindows historicTimeWindows : history) {
                currentTimeWindows.merge(historicTimeWindows);
                merger.incrementMergeCount();
            }
            entry.setValue(currentTimeWindows);
            // }
        }
    }

    private TimeWindows createTimeWindowsForEdge(Edge edge, Collection<GraphChange> graphChanges, double startTime,
            double cycleTime) {
        LinkedList<TimeWindow> timeWindowList = new LinkedList<TimeWindow>();
        TimeWindow timeWindow = new TimeWindow();
        startTime = castToFirstCycle(startTime, cycleTime);
        timeWindow.startTime = startTime;
        double castedTime;
        timeWindow.velocity = lastEdgesValues.get(edge);
        for (GraphChange graphChange : graphChanges) {
            if (graphChange.getEdge() == edge) {
                castedTime = castToFirstCycle(graphChange.getTime(), cycleTime);

                timeWindow.endTime = castedTime;
                timeWindowList.add(timeWindow);

                timeWindow = new TimeWindow();
                timeWindow.startTime = castedTime;
                timeWindow.velocity = graphChange.getVelocity();
            }
        }

        if (timeWindowList.size() > 0 && timeWindowList.getLast().startTime == timeWindow.startTime) {
            timeWindowList.getLast().endTime = castToFirstCycle(startTime, cycleTime) + cycleTime;
        } else {
            timeWindow.endTime = castToFirstCycle(startTime, cycleTime) + cycleTime;
        }
        timeWindowList.add(timeWindow);

        TimeWindows timeWindows = new TimeWindows(new ContiniousAverageMerger());
        timeWindows.setTimeWindows(timeWindowList);
        timeWindows.normalize();
        return timeWindows;
    }

    private double castToFirstCycle(double time, double cycleTime) {
        return time - Math.floor(time / cycleTime) * cycleTime;
    }

    public Map<Edge, TimeWindows> getEdgeCostsMap() {
        return edgeCostsMap;
    }

    public Map<Edge, Double> getLastEdgesValues() {
        return lastEdgesValues;
    }

}
