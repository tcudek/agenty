package agh.edu.pl.graph.changes.estimation;

import java.util.HashMap;
import java.util.Map;

import agh.edu.pl.graph.Edge;
import agh.edu.pl.graph.Graph;
import agh.edu.pl.graph.changes.GraphChange;
import agh.edu.pl.graph.changes.GraphChanges;
import agh.edu.pl.simulation.Simulation;

public class SimpleAverageEstimator implements TimeEstimator {

    private Map<Edge, Double> edgeCostsMap = new HashMap<Edge, Double>();
    private Graph graph;

    public SimpleAverageEstimator(Graph graph, Simulation simulation, GraphChanges graphChanges) {
        this.graph = graph;
        setUpCostMap(graphChanges);
    }

    private void setUpCostMap(GraphChanges graphChanges) {
        for (Edge edge : graph.getEdges()) {
            edgeCostsMap.put(edge, getAverageTime(edge, graphChanges));
        }
    }

    private double getAverageTime(Edge edge, GraphChanges graphChanges) {
        double averageCost = edge.getTime();
        double divisor = 1;
        for (GraphChange graphChange : graphChanges.getGraphChanges()) {
            if (graphChange.getEdge() == edge) {
                averageCost += edge.getDistance() / graphChange.getVelocity();
                ++divisor;
            }
        }
        return averageCost / divisor;
    }

    @Override
    public double estimateTime(double departureTime, Edge edge) {
        return edgeCostsMap.get(edge);
    }
}
