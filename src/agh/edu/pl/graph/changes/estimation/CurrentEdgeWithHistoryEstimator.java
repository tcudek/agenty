package agh.edu.pl.graph.changes.estimation;

import agh.edu.pl.graph.Edge;

public class CurrentEdgeWithHistoryEstimator implements TimeEstimator {

    private TimeWeightedAverageEstimator averageEstimator;

    public CurrentEdgeWithHistoryEstimator(TimeWeightedAverageEstimator averageEstimator) {
        this.averageEstimator = averageEstimator;
    }

    @Override
    public double estimateTime(double departureTime, Edge edge) {
        double averageTime = averageEstimator.estimateTime(departureTime, edge);
        return 0.5 * edge.getTime() + 0.5 * averageTime;
    }
}
