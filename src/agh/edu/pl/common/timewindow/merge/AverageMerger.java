package agh.edu.pl.common.timewindow.merge;

public class AverageMerger implements WindowMerger {

    private double divisor;

    public AverageMerger() {
    }

    public AverageMerger(double divisor) {
        super();
        this.divisor = divisor;
    }

    @Override
    public double mergeVelocity(double velocity1, double velocity2) {
        return (velocity1 + velocity2) / divisor;
    }

    public double getDivisor() {
        return divisor;
    }

    public void setDivisor(double divisor) {
        this.divisor = divisor;
    }

}
