package agh.edu.pl.common.timewindow.merge;

public class ContiniousAverageMerger implements WindowMerger {

    private int mergeCount;

    public void incrementMergeCount() {
        ++mergeCount;
    }

    @Override
    public double mergeVelocity(double velocity1, double velocity2) {
        return velocity1 * (mergeCount + 1) / (mergeCount + 2) + velocity2 / (mergeCount + 2);
    }
}
