package agh.edu.pl.common.timewindow.merge;

public interface WindowMerger {
    public double mergeVelocity(double velocity1, double velocity2);
}
