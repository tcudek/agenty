package agh.edu.pl.common.timewindow;

import java.util.Iterator;

public class TimeWindowsCyclicIterator {

    public TimeWindows timeWindows;
    public Iterator<TimeWindow> iterator;
    public int iteration;

    public TimeWindowsCyclicIterator(TimeWindows timeWindows, int iteration, Iterator<TimeWindow> iterator) {
        this.timeWindows = timeWindows;
        this.iteration = iteration;
        this.iterator = iterator;
    }

    public TimeWindow next() {
        if (!iterator.hasNext()) {
            ++iteration;
            iterator = timeWindows.getTimeWindows().iterator();
        }
        TimeWindow nextTimeWindow = iterator.next();
        TimeWindow timeWindow = nextTimeWindow.clone();
        timeWindow.startTime += iteration * timeWindows.length();
        timeWindow.endTime += iteration * timeWindows.length();
        return timeWindow;
    }
}
