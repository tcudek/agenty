package agh.edu.pl.common.timewindow;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

import agh.edu.pl.common.timewindow.merge.ContiniousAverageMerger;
import agh.edu.pl.common.timewindow.merge.WindowMerger;

public class TimeWindows {

    private LinkedList<TimeWindow> timeWindows;
    private WindowMerger windowMerger;

    public TimeWindows(WindowMerger windowMerger) {
        super();
        this.windowMerger = windowMerger;
    }

    public TimeWindows(TimeWindows timeWindows, WindowMerger windowMerger) {
        this.timeWindows = new LinkedList<TimeWindow>(timeWindows.getTimeWindows());
        this.windowMerger = windowMerger;
    }

    public double length() {
        return timeWindows.getLast().endTime;
    }

    public LinkedList<TimeWindow> getTimeWindows() {
        return timeWindows;
    }

    public void setTimeWindows(LinkedList<TimeWindow> timeWindows) {
        this.timeWindows = timeWindows;
    }

    public void merge(TimeWindows timeWindowsToMerge) {
        ListIterator<TimeWindow> windowsIt = timeWindows.listIterator();
        ListIterator<TimeWindow> windowsToMergeIt = timeWindowsToMerge.getTimeWindows().listIterator();

        TimeWindow window = windowsIt.next();
        TimeWindow windowToMerge = windowsToMergeIt.next();
        while (true) {
            while (true) {
                if (windowToMerge.startTime <= window.endTime) {
                    if (windowToMerge.startTime <= window.startTime) {
                        if (windowToMerge.endTime >= window.endTime) {
                            window.velocity = windowMerger.mergeVelocity(window.velocity, windowToMerge.velocity);
                        }
                        if (windowToMerge.endTime < window.endTime) {
                            windowsIt.previous();
                            windowsIt.add(new TimeWindow(window.startTime, windowToMerge.endTime, windowMerger
                                    .mergeVelocity(window.velocity, windowToMerge.velocity)));
                            windowsIt.next();
                            window.startTime = windowToMerge.endTime;
                        }
                    }
                }
                if (windowToMerge.endTime > window.endTime) {
                    break;
                } else {
                    if (windowsToMergeIt.hasNext()) {
                        windowToMerge = windowsToMergeIt.next();
                    } else {
                        break;
                    }
                }
            }
            if (windowsIt.hasNext()) {
                window = windowsIt.next();
            } else {
                break;
            }
        }
        normalize();
        if (windowMerger instanceof ContiniousAverageMerger) {
            ((ContiniousAverageMerger) windowMerger).incrementMergeCount();
        }
    }

    public void normalize() {
        ListIterator<TimeWindow> windowsIt = timeWindows.listIterator();

        TimeWindow window;
        TimeWindow prevWindow;

        double prevVelocity = -1;
        double prevStartTime = -1;
        if (windowsIt.hasNext()) {
            window = windowsIt.next();
            prevVelocity = window.velocity;
            prevStartTime = window.endTime;
            while (windowsIt.hasNext()) {
                window = windowsIt.next();
                if (window.velocity == prevVelocity) {
                    window.startTime = prevStartTime;
                    windowsIt.previous();
                    windowsIt.previous();
                    windowsIt.remove();
                    window = windowsIt.next();
                }
                prevVelocity = window.velocity;
                prevStartTime = window.startTime;
            }
        }
    }

    // public double estiateTime(double departureTime, double distance) {
    // TimeWindow timeWindow = getTimeWindowForTime(departureTime);
    //
    // double base = Math.floor(departureTime/size()) * size());
    //
    // double velocity = timeWindow.velocity;
    //
    // double arrivalTime = departureTime + distance / velocity ;
    //
    // TimeWindow nextTimeWindow = null;
    // while ((nextTimeWindow = getTimeWindowForTime(arrivalTime)) !=
    // timeWindow)
    // timeWindow = nextTimeWindow;
    // nextTimeWindow = new TimeWindow();
    // arrivalTime = base + timeWindow.startTime
    // + ((distance - velocity * (timeWindow.startTime + base - departureTime))
    // / timeWindow.velocity);
    // } else {
    // break;
    // }
    // graphChangeTmp = changesIt.next();
    // }
    // }

    private TimeWindow getTimeWindowForTime(double departureTime) {
        double time = departureTime - Math.floor(departureTime / length()) * length();
        for (TimeWindow window : timeWindows) {
            if (window.startTime <= departureTime && window.endTime > departureTime) {
                return window;
            }
        }
        return null;
    }

    public TimeWindowsCyclicIterator iterator(double startingTime) {
        int iteration = (int) Math.floor(startingTime / length());
        double baseTime = iteration * length();
        Iterator<TimeWindow> iterator = timeWindows.iterator();
        for (TimeWindow window : timeWindows) {
            if (baseTime + window.startTime <= startingTime && baseTime + window.endTime > startingTime) {
                break;
            }
            iterator.next();
        }
        return new TimeWindowsCyclicIterator(this, iteration, iterator);
    }
}
