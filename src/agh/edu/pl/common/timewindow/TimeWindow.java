package agh.edu.pl.common.timewindow;

public class TimeWindow implements Cloneable {
    public double startTime;
    public double endTime;
    public double velocity;

    public TimeWindow() {
    }

    public TimeWindow(double startTime, double endTime, double velocity) {
        super();
        this.startTime = startTime;
        this.endTime = endTime;
        this.velocity = velocity;
    }

    @Override
    public TimeWindow clone() {
        TimeWindow timeWindow = new TimeWindow(startTime, endTime, velocity);
        return timeWindow;
    }

}
