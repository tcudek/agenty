package agh.edu.pl.common.list;

import java.util.Iterator;
import java.util.LinkedList;

public class EvictableList<E> implements Iterable<E> {

    private LinkedList<E> list = new LinkedList<E>();
    private int maxSize;

    public EvictableList(int maxSize) {
        this.maxSize = maxSize;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void add(E element) {
        list.addFirst(element);
        if (list.size() >= maxSize) {
            list.removeLast();
        }
    }

    public int size() {
        return list.size();
    }

    @Override
    public Iterator<E> iterator() {
        return list.iterator();
    }

}
