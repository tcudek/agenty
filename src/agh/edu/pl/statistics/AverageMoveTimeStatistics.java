package agh.edu.pl.statistics;

public class AverageMoveTimeStatistics {

    private int addCount;
    private double moveTime;
    private String resultName;

    public AverageMoveTimeStatistics() {
    }

    public void addMoveTime(double time) {
        moveTime += time;
        ++addCount;
    }

    public double getAverageMoveTime() {
        return moveTime / addCount;
    }

    public String getResultName() {
        return resultName;
    }

    public void setResultName(String resultName) {
        this.resultName = resultName;
    }
}
