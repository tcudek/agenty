package agh.edu.pl.simulation;

import agh.edu.pl.graph.Graph;
import agh.edu.pl.graph.changes.GraphChange;
import agh.edu.pl.graph.changes.GraphChanges;
import agh.edu.pl.graph.changes.GraphChangesIterator;
import agh.edu.pl.graph.changes.estimation.SimpleAverageEstimator;
import agh.edu.pl.graph.changes.estimation.FullKnowledgeEstimator;
import agh.edu.pl.graph.changes.estimation.StandardEstimator;
import agh.edu.pl.graph.changes.xml.GraphChangesXmlReader;
import agh.edu.pl.graph.xml.GraphXmlReader;
import agh.edu.pl.simulation.action.ChangeGraphAction;
import agh.edu.pl.simulation.action.DepolyTransportUnitAction;
import agh.edu.pl.transport.logic.DecisionAlgorithm;
import agh.edu.pl.transport.logic.DecisionSingleDijskraAlgorithm;
import agh.edu.pl.transport.logic.DecisionIterativeDijskraAlgorithm;

public class SimulationTest {
    public static void main(String[] args) {
        Graph graph = GraphXmlReader.buildGraph("resources/graph1.xml");
        GraphChanges graphChanges = GraphChangesXmlReader.buildGraphChanges("resources/graph1Changes.xml", graph);

        Simulation simulation = new Simulation(graph, 20);

        GraphChangesIterator changesIterator = graphChanges.graphChangesIterator();

        GraphChange graphChange = changesIterator.next();

        ChangeGraphAction changeGraphAction = new ChangeGraphAction(simulation, changesIterator);
        changeGraphAction.setGraphChange(graphChange);
        changeGraphAction.setCycleTime(graphChanges.getCycleTime());

        simulation.addAction(changeGraphAction, graphChange.getTime());

        StandardEstimator timeEstimator = new StandardEstimator();

        DecisionAlgorithm decisionAlgorithm = new DecisionSingleDijskraAlgorithm(graph, timeEstimator);

        DepolyTransportUnitAction deployAction = new DepolyTransportUnitAction(simulation);
        deployAction.setDecisionAlgorithm(decisionAlgorithm);
        deployAction.setNodeA(graph.getNodeById(0));
        deployAction.setNodeB(graph.getNodeById(3));
        deployAction.setTransportUnitName("DijskraClassic TU");

        DecisionAlgorithm decisionAlgorithm2 = new DecisionIterativeDijskraAlgorithm(graph, timeEstimator);

        DepolyTransportUnitAction deployAction2 = new DepolyTransportUnitAction(simulation);
        deployAction2.setDecisionAlgorithm(decisionAlgorithm2);
        deployAction2.setNodeA(graph.getNodeById(0));
        deployAction2.setNodeB(graph.getNodeById(3));
        deployAction2.setTransportUnitName("DijskraIterative TU");

        FullKnowledgeEstimator timeEstimator3 = new FullKnowledgeEstimator();
        timeEstimator3.setGraphChanges(graphChanges);

        DecisionAlgorithm decisionAlgorithm3 = new DecisionSingleDijskraAlgorithm(graph, timeEstimator3);

        DepolyTransportUnitAction deployAction3 = new DepolyTransportUnitAction(simulation);
        deployAction3.setDecisionAlgorithm(decisionAlgorithm3);
        deployAction3.setNodeA(graph.getNodeById(0));
        deployAction3.setNodeB(graph.getNodeById(3));
        deployAction3.setTransportUnitName("DijskraDynamic TU");

        SimpleAverageEstimator timeEstimator4 = new SimpleAverageEstimator(graph, simulation, graphChanges);

        DecisionAlgorithm decisionAlgorithm4 = new DecisionSingleDijskraAlgorithm(graph, timeEstimator4);

        DepolyTransportUnitAction deployAction4 = new DepolyTransportUnitAction(simulation);
        deployAction4.setDecisionAlgorithm(decisionAlgorithm4);
        deployAction4.setNodeA(graph.getNodeById(0));
        deployAction4.setNodeB(graph.getNodeById(3));
        deployAction4.setTransportUnitName("DijskraAverage TU");

        simulation.addAction(deployAction, 0);
        simulation.addAction(deployAction2, 0);
        simulation.addAction(deployAction3, 0);
        simulation.addAction(deployAction4, 0);

        simulation.start();
    }
}
