package agh.edu.pl.simulation;

import agh.edu.pl.graph.Graph;
import agh.edu.pl.graph.changes.GraphChange;
import agh.edu.pl.graph.changes.GraphChanges;
import agh.edu.pl.graph.changes.GraphChangesIterator;
import agh.edu.pl.graph.changes.estimation.CurrentEdgeWithHistoryEstimator;
import agh.edu.pl.graph.changes.estimation.CyclesAverageHistoryEstimator;
import agh.edu.pl.graph.changes.estimation.FullKnowledgeEstimator;
import agh.edu.pl.graph.changes.estimation.SimpleAverageEstimator;
import agh.edu.pl.graph.changes.estimation.StandardEstimator;
import agh.edu.pl.graph.changes.estimation.TimeWeightedAverageEstimator;
import agh.edu.pl.graph.changes.xml.GraphChangesXmlReader;
import agh.edu.pl.graph.xml.GraphXmlReader;
import agh.edu.pl.simulation.action.ChangeGraphAction;
import agh.edu.pl.simulation.action.DepolyTransportUnitAction;
import agh.edu.pl.simulation.action.UpdateClassifierAction;
import agh.edu.pl.statistics.AverageMoveTimeStatistics;
import agh.edu.pl.transport.logic.DecisionAlgorithm;
import agh.edu.pl.transport.logic.DecisionIterativeDijskraAlgorithm;
import agh.edu.pl.transport.logic.DecisionSingleDijskraAlgorithm;

public class SimulationTest2 {
    public static void main(String[] args) {
        Graph graph = GraphXmlReader.buildGraph("resources/graph3.xml");
        GraphChanges graphChanges = GraphChangesXmlReader.buildGraphChanges("resources/graph3Changes.xml", graph);

        Simulation simulation = new Simulation(graph, 100);

        GraphChangesIterator changesIterator = graphChanges.graphChangesIterator();

        GraphChange graphChange = changesIterator.next();

        ChangeGraphAction changeGraphAction = new ChangeGraphAction(simulation, changesIterator);
        changeGraphAction.setGraphChange(graphChange);
        changeGraphAction.setCycleTime(graphChanges.getCycleTime());

        simulation.addAction(changeGraphAction, graphChange.getTime());

        StandardEstimator standardEstimator = new StandardEstimator();

        CyclesAverageHistoryEstimator cycleClassifier = new CyclesAverageHistoryEstimator();
        cycleClassifier.buildClassifier(graph.getEdges());

        UpdateClassifierAction updateClassifierAction = new UpdateClassifierAction(simulation);
        updateClassifierAction.setClassifier(cycleClassifier);
        updateClassifierAction.setCycleTime(12);
        updateClassifierAction.setGraphChanges(graphChanges);

        simulation.addAction(updateClassifierAction, 12);

        AverageMoveTimeStatistics statistics = new AverageMoveTimeStatistics();
        statistics.setResultName("Result");

        AverageMoveTimeStatistics statistics1 = new AverageMoveTimeStatistics();
        statistics1.setResultName("Single dijskra");

        AverageMoveTimeStatistics statistics2 = new AverageMoveTimeStatistics();
        statistics2.setResultName("Iterative dijskra");

        AverageMoveTimeStatistics statistics3 = new AverageMoveTimeStatistics();
        statistics3.setResultName("Average dijskra");

        AverageMoveTimeStatistics statistics4 = new AverageMoveTimeStatistics();
        statistics4.setResultName("Average itarative dijskra");

        AverageMoveTimeStatistics statistics5 = new AverageMoveTimeStatistics();
        statistics5.setResultName("Average, time weights dijskra");

        AverageMoveTimeStatistics statistics6 = new AverageMoveTimeStatistics();
        statistics6.setResultName("Average, current most important dijskra");

        AverageMoveTimeStatistics statistics7 = new AverageMoveTimeStatistics();
        statistics7.setResultName("Average, current most important iterative dijskra");

        AverageMoveTimeStatistics statistics8 = new AverageMoveTimeStatistics();
        statistics8.setResultName("Full knowledge dijskra");

        AverageMoveTimeStatistics statistics9 = new AverageMoveTimeStatistics();
        statistics9.setResultName("Learning dijskra");

        for (int i = 0; i < 60; ++i) {

            DecisionAlgorithm decisionAlgorithm = new DecisionSingleDijskraAlgorithm(graph, standardEstimator);

            DepolyTransportUnitAction deployAction1 = new DepolyTransportUnitAction(simulation);
            deployAction1.setDecisionAlgorithm(decisionAlgorithm);
            deployAction1.setNodeA(graph.getNodeById(1));
            deployAction1.setNodeB(graph.getNodeById(16));
            deployAction1.setTransportUnitName("DijskraSingle TU");
            deployAction1.setStatistics(statistics1);

            DecisionAlgorithm decisionAlgorithm2 = new DecisionIterativeDijskraAlgorithm(graph, standardEstimator);

            DepolyTransportUnitAction deployAction2 = new DepolyTransportUnitAction(simulation);
            deployAction2.setDecisionAlgorithm(decisionAlgorithm2);
            deployAction2.setNodeA(graph.getNodeById(1));
            deployAction2.setNodeB(graph.getNodeById(16));
            deployAction2.setTransportUnitName("DijskraIterative TU");
            deployAction2.setStatistics(statistics2);

            SimpleAverageEstimator estimator3 = new SimpleAverageEstimator(graph, simulation, graphChanges);

            DecisionAlgorithm decisionAlgorithm3 = new DecisionSingleDijskraAlgorithm(graph, estimator3);

            DepolyTransportUnitAction deployAction3 = new DepolyTransportUnitAction(simulation);
            deployAction3.setDecisionAlgorithm(decisionAlgorithm3);
            deployAction3.setNodeA(graph.getNodeById(1));
            deployAction3.setNodeB(graph.getNodeById(16));
            deployAction3.setTransportUnitName("DijskraSimpleAverage TU");
            deployAction3.setStatistics(statistics3);

            SimpleAverageEstimator estimator4 = new SimpleAverageEstimator(graph, simulation, graphChanges);

            DecisionAlgorithm decisionAlgorithm4 = new DecisionSingleDijskraAlgorithm(graph, estimator4);

            DepolyTransportUnitAction deployAction4 = new DepolyTransportUnitAction(simulation);
            deployAction4.setDecisionAlgorithm(decisionAlgorithm4);
            deployAction4.setNodeA(graph.getNodeById(1));
            deployAction4.setNodeB(graph.getNodeById(16));
            deployAction4.setTransportUnitName("IterativeDijskraSimpleAverage TU");
            deployAction4.setStatistics(statistics4);

            TimeWeightedAverageEstimator estimator5 = new TimeWeightedAverageEstimator(graph, simulation, graphChanges,
                    simulation.getSimulationEndTime());

            DecisionAlgorithm decisionAlgorithm5 = new DecisionSingleDijskraAlgorithm(graph, estimator5);

            DepolyTransportUnitAction deployAction5 = new DepolyTransportUnitAction(simulation);
            deployAction5.setDecisionAlgorithm(decisionAlgorithm5);
            deployAction5.setNodeA(graph.getNodeById(1));
            deployAction5.setNodeB(graph.getNodeById(16));
            deployAction5.setTransportUnitName("DijskraWeightedAverage TU");
            deployAction5.setStatistics(statistics5);

            TimeWeightedAverageEstimator subEstimator6 = new TimeWeightedAverageEstimator(graph, simulation,
                    graphChanges, simulation.getSimulationEndTime());
            CurrentEdgeWithHistoryEstimator estimator6 = new CurrentEdgeWithHistoryEstimator(subEstimator6);

            DecisionAlgorithm decisionAlgorithm6 = new DecisionSingleDijskraAlgorithm(graph, estimator6);

            DepolyTransportUnitAction deployAction6 = new DepolyTransportUnitAction(simulation);
            deployAction6.setDecisionAlgorithm(decisionAlgorithm6);
            deployAction6.setNodeA(graph.getNodeById(1));
            deployAction6.setNodeB(graph.getNodeById(16));
            deployAction6.setTransportUnitName("DijskraFirstImportantAverage TU");
            deployAction6.setStatistics(statistics6);

            TimeWeightedAverageEstimator subEstimator7 = new TimeWeightedAverageEstimator(graph, simulation,
                    graphChanges, simulation.getSimulationEndTime());
            CurrentEdgeWithHistoryEstimator estimator7 = new CurrentEdgeWithHistoryEstimator(subEstimator7);

            DecisionAlgorithm decisionAlgorithm7 = new DecisionIterativeDijskraAlgorithm(graph, estimator7);

            DepolyTransportUnitAction deployAction7 = new DepolyTransportUnitAction(simulation);
            deployAction7.setDecisionAlgorithm(decisionAlgorithm7);
            deployAction7.setNodeA(graph.getNodeById(1));
            deployAction7.setNodeB(graph.getNodeById(16));
            deployAction7.setTransportUnitName("IterativeDijskraFirstImportantAverage TU");
            deployAction7.setStatistics(statistics7);

            FullKnowledgeEstimator estimator8 = new FullKnowledgeEstimator();
            estimator8.setGraphChanges(graphChanges);

            DecisionAlgorithm decisionAlgorithm8 = new DecisionSingleDijskraAlgorithm(graph, estimator8);

            DepolyTransportUnitAction deployAction8 = new DepolyTransportUnitAction(simulation);
            deployAction8.setDecisionAlgorithm(decisionAlgorithm8);
            deployAction8.setNodeA(graph.getNodeById(1));
            deployAction8.setNodeB(graph.getNodeById(16));
            deployAction8.setTransportUnitName("FullyKnowledgeAlgorithm TU");
            deployAction8.setStatistics(statistics8);

            DecisionAlgorithm decisionAlgorithm9 = new DecisionSingleDijskraAlgorithm(graph, cycleClassifier);

            DepolyTransportUnitAction deployAction9 = new DepolyTransportUnitAction(simulation);
            deployAction9.setDecisionAlgorithm(decisionAlgorithm9);
            deployAction9.setNodeA(graph.getNodeById(1));
            deployAction9.setNodeB(graph.getNodeById(16));
            deployAction9.setTransportUnitName("Learning Dijskra TU" + i);
            deployAction9.setStatistics(statistics9);

            simulation.addAction(deployAction1, i);
            simulation.addAction(deployAction2, i);
            simulation.addAction(deployAction3, i);
            simulation.addAction(deployAction4, i);
            simulation.addAction(deployAction5, i);
            simulation.addAction(deployAction6, i);
            simulation.addAction(deployAction7, i);
            simulation.addAction(deployAction8, i);
            simulation.addAction(deployAction9, i);

        }
        simulation.start();

        printlnAverageTime(statistics1);
        printlnAverageTime(statistics2);
        printlnAverageTime(statistics3);
        printlnAverageTime(statistics4);
        printlnAverageTime(statistics5);
        printlnAverageTime(statistics6);
        printlnAverageTime(statistics7);
        printlnAverageTime(statistics8);
        printlnAverageTime(statistics9);
    }

    private static void printlnAverageTime(AverageMoveTimeStatistics statistics) {
        System.out.println(statistics.getResultName() + " - average time: " + statistics.getAverageMoveTime());
    }
}
