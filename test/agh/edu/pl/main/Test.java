package agh.edu.pl.main;

import java.util.Collection;

import agh.edu.pl.graph.Edge;
import agh.edu.pl.graph.Graph;
import agh.edu.pl.graph.algorithm.DijskraAlgorithm;
import agh.edu.pl.graph.changes.GraphChanges;
import agh.edu.pl.graph.changes.estimation.StandardEstimator;
import agh.edu.pl.graph.changes.xml.GraphChangesXmlReader;
import agh.edu.pl.graph.xml.GraphXmlReader;

public class Test {

    public static void main(String[] args) {
        Graph graph = GraphXmlReader.buildGraph("resources/graph1.xml");
        GraphChanges graphChanges = GraphChangesXmlReader.buildGraphChanges("resources/graph1Changes.xml", graph);

        StandardEstimator estimator = new StandardEstimator();

        DijskraAlgorithm dijskra = new DijskraAlgorithm(graph);
        dijskra.setTimeEstimator(estimator);

        Collection<Edge> path1 = dijskra.getPath(graph.getNodeById(0), graph.getNodeById(3), 0);

        Collection<Edge> path2 = dijskra.getPath(graph.getNodeById(0), graph.getNodeById(4), 0);
        return;
    }
}
