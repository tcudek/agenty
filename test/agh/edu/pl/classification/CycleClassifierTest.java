package agh.edu.pl.classification;

import agh.edu.pl.graph.Graph;
import agh.edu.pl.graph.changes.GraphChanges;
import agh.edu.pl.graph.changes.estimation.CyclesAverageHistoryEstimator;
import agh.edu.pl.graph.changes.xml.GraphChangesXmlReader;
import agh.edu.pl.graph.xml.GraphXmlReader;

public class CycleClassifierTest {

    public static void main(String[] args) {
        Graph graph = GraphXmlReader.buildGraph("resources/graph1.xml");
        GraphChanges graphChanges = GraphChangesXmlReader.buildGraphChanges("resources/graph1Changes.xml", graph);

        CyclesAverageHistoryEstimator classifier = new CyclesAverageHistoryEstimator();
        classifier.buildClassifier(graph.getEdges());
        classifier.updateClassificator(graphChanges, 4, 4);

        classifier.updateClassificator(graphChanges, 8, 4);

        classifier.updateClassificator(graphChanges, 12, 4);

        classifier.updateClassificator(graphChanges, 16, 4);

        return;
    }
}
