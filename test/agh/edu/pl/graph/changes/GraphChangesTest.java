package agh.edu.pl.graph.changes;

import java.util.Collection;

import agh.edu.pl.graph.Graph;
import agh.edu.pl.graph.changes.xml.GraphChangesXmlReader;
import agh.edu.pl.graph.xml.GraphXmlReader;

public class GraphChangesTest {

    public static void main(String[] args) {
        Graph graph = GraphXmlReader.buildGraph("resources/graph1.xml");
        GraphChanges graphChanges = GraphChangesXmlReader.buildGraphChanges("resources/graph1Changes.xml", graph);

        Collection<GraphChange> changes = graphChanges.getGraphChangesToTime(100);

        Collection<GraphChange> changes2 = graphChanges.getGraphChangesBetweenTimes(20, 40);
        return;
    }

}
