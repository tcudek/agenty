package agh.edu.pl.common.timewindow;

import java.util.LinkedList;

import agh.edu.pl.common.timewindow.merge.ContiniousAverageMerger;

public class TimeWindowTest {

    public static void main(String[] args) {
        LinkedList<TimeWindow> timeWindowsList1 = new LinkedList<TimeWindow>();
        LinkedList<TimeWindow> timeWindowsList2 = new LinkedList<TimeWindow>();

        timeWindowsList1.add(new TimeWindow(0, 1, 2));
        timeWindowsList1.add(new TimeWindow(1, 3, 4));
        timeWindowsList1.add(new TimeWindow(3, 5, 6));
        timeWindowsList1.add(new TimeWindow(5, 7, 2));
        timeWindowsList1.add(new TimeWindow(7, 10, 4));
        timeWindowsList1.add(new TimeWindow(10, 11, 6));
        timeWindowsList1.add(new TimeWindow(11, 14, 4));

        timeWindowsList2.add(new TimeWindow(0, 4, 6));
        timeWindowsList2.add(new TimeWindow(4, 7, 4));
        timeWindowsList2.add(new TimeWindow(7, 8, 2));
        timeWindowsList2.add(new TimeWindow(8, 11, 4));
        timeWindowsList2.add(new TimeWindow(11, 14, 6));

        TimeWindows timeWindows1 = new TimeWindows(new ContiniousAverageMerger());
        TimeWindows timeWindows2 = new TimeWindows(new ContiniousAverageMerger());

        timeWindows1.setTimeWindows(timeWindowsList1);
        timeWindows2.setTimeWindows(timeWindowsList2);

        timeWindows1.merge(timeWindows2);
        return;
    }
}
